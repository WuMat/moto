module.exports = {
  username: process.env.USERNAME,
  password: process.env.PASSWORD,
  platform: 'bitbucket',
  baseDir: `${process.env.BITBUCKET_CLONE_DIR}/renovate`,
  repositories: [ "WuMat/backmoto" ],
}