import React from "react";
import classNames from 'classnames';
import isArray from 'lodash/isArray';
import { Link } from 'react-router-dom'
import { fetchPlace } from 'data/places';

import { PanelAlert } from 'components/PanelAlert';
import { Button } from 'components/Button';
import { PanelInfo } from 'components/PanelInfo';
import { PanelInfoData } from 'components/PanelInfoData';
import { Rating} from 'components/Rating';




export class HomePage extends React.PureComponent {
 constructor(props){
   super(props);

   this.state = {
     working: true,
     places: []
    }
 }

  componentDidMount(){
    fetchPlace('places?per_page=3')
    .then((payload) => this.setState({ payload, working: false}))
    .catch((error) => this.setState({ error, working: false }))
    
  }

  
  renderList = () => {
    const { payload } = this.state;
    const arr = payload.places;

    return isArray(arr) && arr.map((place) =>( 
    <PanelInfoData key={place._id} namePlace={place.name} img={place.placeImages[0]}>
     <Rating id={place._id} max={5} value={0} oneStar={place.oneStar} twoStar={place.twoStar} threeStar={place.threeStar} fourStar={place.fourStar} fiveStar={place.fiveStar}/>
    </PanelInfoData>))
    
    }

    render() {

    const baseClass = "homePage";
    const classes = classNames(baseClass);

    const { working, payload, error } = this.state;
 
    return (
     <div className={classes}>
      <main>
        <PanelAlert  color={"gray"}>
          Witaj! Nie masz jeszcze swojego konta? Zostań jednym z nas!
          <Button label={'Załóż swoje konto'} color={'gray'} type={'button'} icon={'fas fa-users'} size={'bg'}></Button>
        </PanelAlert>

        <PanelInfo label={'najpopularniejsze miejsca'} icon={'far fa-star'}>
          {payload && this.renderList()}
          {/* <PanelInfoData namePlace={'gwarus'}>
            <Rating quantityRange={'ile tam ocen'} max={5} value={3} />
          </PanelInfoData> */}
  
        </PanelInfo>

        <PanelAlert color={"orange"}>
          Byłeś ostatnio w ciekawym miejscy i chcesz je polecić innym? 
          <Link to='/add_place'><Button label={'Dodaj je do naszej bazy'} color={'orange'} type={'button'} icon={'fas fa-plus-circle'} size={'bg'} ></Button></Link>
        </PanelAlert>

        <PanelInfo label={'miejsca dodane w tym tygodniu'} icon={'fas fa-road'}>
          {payload && this.renderList()}
        </PanelInfo>

        <div className='bottom'>
          <div className='stats'>
            <PanelInfo label={'Statystyki'} icon={'fas fa-chart-line'}>
              Statsy
                {/* {working && <span> ... </span>}
                {payload && this.renderList()}
                {error && <span> ERROR </span>} */}
            </PanelInfo>
          </div>
          <div className='activity'>
            <PanelInfo label={'Aktywność'} icon={'fas fa-align-center'}>
              {/* {working && <span> ... </span>}
              {payload && this.renderList()}
              {error && <span> ERROR </span>} */}
            </PanelInfo>
          </div>
        </div>
      </main>
      <aside>
        <PanelInfo label='Reklama' icon={'fas fa-info'}></PanelInfo>
      </aside>
    </div>
    )
  }
}