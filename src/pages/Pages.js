import React from 'react';
import { Route, Switch } from 'react-router-dom';

import { HomePage } from "pages/Home_Page";
import { BestsPlace } from "pages/BestsPlace";
import { AddPlace} from 'pages/AddPlace';
import { NewPlaces } from 'pages/NewPlaces';

import { Layout } from "components";
import { Content } from 'components/Content';

export class Pages extends React.PureComponent {
	render() {
		return (
			<Layout>
				<Content>
					<Switch>
						<Route path="/home_page" component={HomePage} />
						<Route path="/bests_place" component={BestsPlace} />
						<Route path="/new_places" component={NewPlaces} />
						<Route path='/add_place' component={AddPlace} />
					</Switch>
				</Content>
			</Layout>
		)
	}
}
