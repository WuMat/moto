import React from 'react';
import classNames from 'classnames';

import { PanelInfo } from 'components/PanelInfo'
import { ViewPlacesBig } from 'components/ViewPlacesBig'
import { fetchPlace } from 'data/places';

export class NewPlaces extends React.PureComponent{
  constructor(props){
    super(props)

    this.state={
      placesOnPage: 5,
      count: 1,
      payload: null
    }
  }

  componentDidMount(){
    fetchPlace(`places?per_page=${this.state.placesOnPage * this.state.count}`)
    .then((payload) => this.setState({payload, working: false}))
    .catch((error) => this.setState({error, working: false}))
  }
  

  render(){
    console.log(this.state.payload);
    const baseClass = 'newPlaces';
    const classes = classNames(baseClass)
    return(
      <div className={classes}>
        <PanelInfo label={'Najnowsze dodane miejsca !'} icon={'fas fa-flag'}>
          <div className="wrap">
            <ViewPlacesBig></ViewPlacesBig>
            <ViewPlacesBig></ViewPlacesBig>
            <ViewPlacesBig></ViewPlacesBig>
          </div>
        </PanelInfo>      
      </div>
    )
  }
}