import React from 'react';
import classNames from 'classnames';

import { PanelInfo } from 'components/PanelInfo';
import { FormAddPlace } from 'components/FormAddPlace'

export class AddPlace extends React.PureComponent{

  render(){

    const baseClass = 'addPlace';
    const classes = classNames(baseClass)
    return(
      <div className={classes}>
        <PanelInfo label={"Dodaj nowe miejsce do bazy danych"} icon={'fas fa-plus-circle'}>
          <div className={'formWrap'}>
            <FormAddPlace></FormAddPlace>
          </div>
        </PanelInfo>
      </div>

    )
  }
}