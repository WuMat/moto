import React from 'react'
import classNames from 'classnames'
import axios from 'axios';

export class Rating extends React.PureComponent{
  constructor(props){
    super(props)
      this.state = {
        max: this.props.max,
        value: this.valueRating(),
        rating: this.viewRating(this.props.max, this.valueRating()),
        quantityRange: this.evaulateRating().sum
      }
  }

  evaulateRating(){
    const one = this.props.oneStar;
    const two = this.props.twoStar;
    const three = this.props.threeStar;
    const four = this.props.fourStar;
    const five = this.props.fiveStar;

    const sum = one + two + three + four + five
    const multiply =  ((one * 1) + (two * 2) + (three * 3) + (four * 4) + (five * 5))
    return {
      sum,
      multiply
    }
  }

  valueRating(){
    const quant = this.evaulateRating()
    const sum = quant.sum;
    const multiply = quant.multiply
    const value = Math.round(multiply / sum)
  
    console.log(multiply / sum)
    return value
  }
  

  viewRating(max, value){
    return [...Array(value).fill(true), ...Array(max-value).fill(false)] 
  }

  clickRating(i){
    // this.setState({
    //   value: i
    // });
    
    let form = []
    switch(i){
      case 1:
        form.push({"propName": "oneStar", "value": `${this.props.oneStar + 1}`})
      break;
      case 2:
        form.push({"propName": "twoStar", "value": `${this.props.twoStar + 1}`});
      break;
      case 3:
        form.push({"propName": "threeStar", "value": `${this.props.threeStar + 1}`});
      break;
      case 4:
        form.push({"propName": "fourStar", "value": `${this.props.fourStar + 1}`});
      break;
      case 5:
        form.push({"propName": "fiveStar", "value": `${this.props.fiveStar + 1}`});
      break;
      }

    const id = this.props.id 
    axios.patch(`/api/places/${id}`, form)
    .then(response=>console.log(response))

  }

  onMouseEnter(i){
    return ()=> this.setRating(i)
  }

  onMouseLeave(i){
    return ()=> this.setRating(this.state.value)
   
  }

  setRating(i){
    this.setState({
      rating: this.viewRating(this.state.max, i)
    })
  }

  onClick(i){
    return ()=> this.clickRating(i)
  }


  render(){

    const baseClass = 'rating'
    const classes = classNames(baseClass)

    const{ quantityRange } = this.state;
    
    return(
      <div className={classes}>
        {this.state.rating.map((star, i) => (<span key={i} className={star ? 'icon true' : 'icon false'}
        onMouseEnter={this.onMouseEnter(i+1)} 
        onMouseLeave={this.onMouseLeave(i+1)}
        onClick={this.onClick(i+1)}
        ><i className="fas fa-star"></i></span>))}
        <p>{quantityRange === 1 ? `${quantityRange}   ocena`: 
          quantityRange > 1 && quantityRange < 5 ? `${quantityRange}   oceny` :
          `${quantityRange}   ocen` }</p>
      </div>
    )
  }
}