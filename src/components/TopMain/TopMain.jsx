import React from 'react';
import classNames from "classnames"; 

import {Nav} from "./../Nav"
import {Button} from "./../Button"

export const TOP_MAIN_ID = 'topMain' 

export class TopMain extends React.PureComponent{
  render(){

    const {children} = this.props;

    const baseClass = "topMain";
    const classes = classNames(baseClass);

    return(
      <div>
        <div className={classes}>
          <div className="center">
            <div className="left">
              MIEJSCA KTÓRE WARTO ZWIEDZIĆ &nbsp;&nbsp;&nbsp;//&nbsp;&nbsp;&nbsp;GdziePojade.pl
            </div>
            <div className="right">
            <Button label="Załóż swoje konto" color="transparent"/>
            <Button label="zaloguj się" size="sm" color="orange" icon="fas fa-unlock-alt" />
            </div>
            
          </div>
        </div>
        <div className={"navBar"}>
          <div className={"center"}>
            {children}
          </div>
        </div>
      </div>
    )
  }
}