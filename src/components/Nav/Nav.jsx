import React from 'react';
import classNames from "classnames";
import { Link } from 'react-router-dom'


export class Nav extends React.PureComponent{
  render(){

    const baseClass = "nav";
    const classes = classNames(baseClass);


    return(
      <div className={classes}>
        <ul>
          <li><Link to="/home_page"><span></span><span></span>Strona Główna</Link></li>
          <li><Link to="#"><span></span><span></span>Użytkownicy</Link></li>
          <li><Link to="/bests_place"><span></span><span></span>najlepiej oceniane miejsca</Link></li>
          <li><Link to="/new_places"><span></span><span></span>Nowo dodane miejsca</Link></li>
          <li><Link to="/add_place"><span></span><span></span>Dodaj miejsce</Link></li>
          <li><Link to="#"><span></span><span></span>Szukaj</Link></li>
        </ul>
      </div>
      
    )
  }
}