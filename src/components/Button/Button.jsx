import React from 'react';
import classNames from "classnames";
import PropTypes from "prop-types";

export class Button extends React.PureComponent{
  

  static defaultProps = {
  label: "cos tam",
  }

  static propTypes = {
  label: PropTypes.string,
  color: PropTypes.oneOf(["gray", "orange", "transparent"]),
  type: PropTypes.oneOf(["button", "submit", "reset"]),
  size: PropTypes.oneOf(["bg","sm", "res"]),
  border: PropTypes.oneOf(['orange', 'gray'])
  }



  render(){
     const {color, border, icon, label, type, size, onClick, top} = this.props;
    
    const baseClass = "btn";
    const classes = classNames(
      baseClass,
    {
      [`${baseClass}--color-${color}`]: !!color,
    },
    {
      [`${baseClass}--size-${size}`]: !!size,
    },
    {
      [`${baseClass}--border-${border}`]: !!border
    }
    );
    
    return(
      <button className={classes} type={type} onClick={onClick} style={{marginTop:`${top}px`}}>
        {icon ? <span className="icon"><i className={icon} ></i></span>: null}
        {label.toUpperCase()}
      </button>
    );
  }
}
