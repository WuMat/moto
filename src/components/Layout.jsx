import React from 'react';
import classNames from "classnames";

import { TopContent } from "./TopContent";
import { TopScroll} from "./TopScroll";
import { Nav } from './Nav';
import { TopMain } from './TopMain';
import { Content } from './Content';


export class Layout extends React.PureComponent{
  
  render(){
    const {children} = this.props;
    
    const baseClass = "layout";
    const classes = classNames(baseClass);
    

    return(
      <div className={classes}>
        <TopContent>
          <TopScroll />
        </TopContent>
        <TopMain>
          <Nav />
        </TopMain>
        <Content>
          {children}
        </Content>
      </div>
    );
  }
}


