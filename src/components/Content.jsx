import React from 'react'

export class Content extends React.PureComponent {

  render(){
    const {children} = this.props;

    return(
      <div>
        {children}
      </div>
    )
  }
}