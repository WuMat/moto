import React from 'react';
import classNames from "classnames";



export class TopContent extends React.PureComponent{
  render(){
    const {children} = this.props;

    const baseClass = "topContent";
    const classes = classNames(baseClass);

    return(
      <div className={classes}><p>GdziePojade .PL</p>
      {children}
      </div>
    )
  }
}
