import React from 'react';
import classNames from "classnames"


export class TopScroll extends React.PureComponent{

  handleClick() {
    const ofs = document.querySelector('.topMain');
    const windowHeight = window.innerHeight;
    const height = ofs.getBoundingClientRect().top;
    let start = windowHeight - height;
    
    const frame = () => {
      if(start >= windowHeight){
        clearInterval(scroll)
      }
      else { 
        start += 15;
        window.scrollTo(0, start);
      }
    }
    const scroll = setInterval(frame, 1);
  }

  render(){

    const baseClass = "topScroll";
    const classes = classNames(baseClass);

    return(
      <div className={classes}>
        <h1>KLIKNIJ I ZOBACZ CO NA CIEBIE CZEKA</h1>
        <div className="scroll" onClick={this.handleClick}>
          <span className="topScroll"></span> 
          <span className="middleScroll"></span> 
          <span className="bottomScroll"></span>
          <span className="arrow"></span> 
        </div>
      </div>
    )
  }
}
