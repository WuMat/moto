import React from 'react'
import classNames from 'classnames'

export class ViewPlacesBig extends React.PureComponent{
  render(){

    const baseClass = 'viewPlacesBig'
    const classes = classNames(baseClass)
    return(
      <div className={classes}>
        <div className="photo"></div>
        <div className="info"></div>
        <div className="map"></div>
      </div>
    )
  }
}