import React from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types'

export class PanelInfo extends React.PureComponent{

  static propTypes = {
    label: PropTypes.string.isRequired
  }

  render(){

    const{ icon, label, children }=this.props;

    const baseClass = 'panelInfo';
    const classes = classNames(baseClass);


    return(
      <div className={classes}>
        <div className='up'>
          <span className='icon'>{icon ? <i className={icon}></i>: 'ICON'}</span>
          <span className='description'>{label.toUpperCase()}</span>
        </div>
        <div className='down'>
          {children}
        </div>
      </div>
    )
  }
}