import React from 'react';
import classNames from 'classnames';
import propTypes from 'prop-types';


export class PanelInfoData extends React.PureComponent{
 
  
  render(){

    const {namePlace, children, img} = this.props;

    const baseClass = `panelInfoData`;
    const classes = classNames(baseClass);
    // console.log(img);
  

    return(
      <div className={classes}>
        <div className="oneData">
          <div className="img"><img src={`http://localhost:8080/${img}`} /></div>
          <div className="info">
            <p>{namePlace}</p>
            <p>786 od twojej lokalizajci</p>
            {children}
          
          </div>
        </div>
      </div>
    )
  }
}