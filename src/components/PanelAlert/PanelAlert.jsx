import React from 'react';
import classNames from "classnames";
import PropTypes from "prop-types";

export class PanelAlert extends React.PureComponent{
  static defaultProps = {
    color: "gray"
  }

  static propTypes = {
    color: PropTypes.oneOf(["gray", "orange"])
  };

  render(){

    const { children, color } = this.props;

    const baseClass = "panelAlert";
    const classes = classNames(
      baseClass,
      {
        [`${baseClass}--color-${color}`]: !!color,
      }
    );


    return(
      <div className={classes}>
      {children}
      </div>
    );
  }
}