import React from 'react';
import classNames from 'classnames';
import axios from 'axios';

import { Button } from 'components/Button';
import { PanelInfo} from 'components/PanelInfo'

export class FormAddPlace extends React.PureComponent{
  constructor(props){
    super(props)
 

    this.state ={
      namePlace: '',
      city: '',
      cosik:'',
      province:'',
      description: '',
      kindOfPlace: '',
      file:'',
      namePlaceValid: '',
      cityValid: '',
      provinceValid: '',
      kindOfPlaceValid: '',
      descriptionValid: '',
      selectedFile1: null,
      selectedFile2: null,
      selectedFile3: null,
      selectedFile4: null,
      selectedFile5: null,
      selectedFile6: null,
      imagePreviewUrl1: '',
      imagePreviewUrl2: '',
      imagePreviewUrl3: '',
      imagePreviewUrl4: '',
      imagePreviewUrl5: '',
      imagePreviewUrl6: '',
    }
  }

  handleSelectFile = (e) =>{
    let reader = new FileReader();
    const name = e.target.name;
    let file = e.target.files[0];
    
    // console.log(name)
    // console.log(e.target.files[0])
    reader.onloadend = () =>{ 
      this.setState({
        // file: file,
        [name]: reader.result
      });
    }
    reader.readAsDataURL(file)
    if(name==='imagePreviewUrl1'){this.setState({selectedFile1: file})}
    if(name==='imagePreviewUrl2'){this.setState({selectedFile2: file})}
    if(name==='imagePreviewUrl3'){this.setState({selectedFile3: file})}
    if(name==='imagePreviewUrl4'){this.setState({selectedFile4: file})}
    if(name==='imagePreviewUrl5'){this.setState({selectedFile5: file})}
    if(name==='imagePreviewUrl6'){this.setState({selectedFile6: file})}
  }

  handleChange = (e) => {
    const name = e.target.name;
    const value = e.target.value
    this.setState({
      [name]: value
    }, 
    ()=>{this.validateField(name, value)});

  }

  validateField = (name, value) =>{
    let quntity = value.length;
    switch(name){
      case 'namePlace':
        this.setState({
          namePlaceValid: quntity === 0 ? '' : quntity > 7 ? true : false
        })
      break;
      case 'city':
        this.setState({
          cityValid: quntity === 0 ? '' : quntity > 4 ? true : false
        })
      break;
      case 'province':
        this.setState({
          provinceValid: value === 'Wybierz Wojewodztwo' ? false : true
        })
      break;
      case 'kindOfPlace':
        this.setState({
          kindOfPlaceValid: value === 'Wybierz rodzaj miejsca' ? false : true
        })
      break;
      case 'description':
        this.setState({
          descriptionValid: quntity === 0 ? '' : quntity > 60 ? true : false
        })
    }
  }


  handleSubmit = (e) => {
    e.preventDefault();
    if(
      !this.state.namePlaceValid || this.state.namePlaceValid === '' ||
      !this.state.cityValid || this.state.cityValid === '' ||
      !this.state.provinceValid || this.state.provinceValid === '' ||
      !this.state.kindOfPlaceValid || this.state.kindOfPlaceValid === '' ||
      !this.state.descriptionValid || this.state.descriptionValid === '' )
      return console.log('popraw dane');

    const data = new FormData();
    data.append('name', this.state.namePlace);
    data.append('province', this.state.province);
    data.append('kindOfPlace', this.state.kindOfPlace);
    data.append('description', this.state.description);
    data.append('oneStar', 0)
    data.append('twoStar', 0)
    data.append('threeStar', 0)
    data.append('fourStar', 0)
    data.append('fiveStar', 0)
    data.append('grade', 0)
    data.append('placeImages', this.state.selectedFile1, this.state.selectedFile1.name);
    data.append('placeImages', this.state.selectedFile2, this.state.selectedFile2.name);
    data.append('placeImages', this.state.selectedFile3, this.state.selectedFile3.name);
    data.append('placeImages', this.state.selectedFile4, this.state.selectedFile4.name);
    data.append('placeImages', this.state.selectedFile5, this.state.selectedFile5.name);
    data.append('placeImages', this.state.selectedFile6, this.state.selectedFile6.name);

    console.log(data);

    axios.post('/api/places', data, {
      onUploadProgress: progressEvent => {
        console.log(`upload Progress: ${Math.round(progressEvent.loaded / progressEvent.total * 100)} %`);
      }
    })
    .then(response => console.log(response))
    .catch(err => console.log(err))
  }
 
  render(){
    const validAnswer = 'Prawidłowe';
    const baseClass = 'formAddPlace';
    const classes = classNames(baseClass);

    const {imagePreviewUrl1, imagePreviewUrl2, imagePreviewUrl3, imagePreviewUrl4, imagePreviewUrl5, imagePreviewUrl6} = this.state
   


    return(
      <div className={classes}>
        <form id='form' action="" onSubmit={this.handleSubmit} noValidate>
          <div className="form-group">

            <div>
              <input type="text" name='namePlace' placeholder='Podaj nazwę miejsca' 
              onChange={this.handleChange}/>
              {this.state.namePlaceValid ? <div className="valid valid-acces">{validAnswer}</div>
              : this.state.namePlaceValid ===''? null:<div className="valid valid-error">Podaj nazwę miejsca, min. 7 znaków</div>}
            </div>

            <div>
              <input type="text" name='city' placeholder='Miejscowość' data-error-message='Podaj miejscowość' 
              onChange={this.handleChange}/>
              {this.state.cityValid ? <div className="valid valid-acces">{validAnswer}</div>
              : this.state.cityValid ===''? null:<div className="valid valid-error">Podaj nazwę miejscowości, min 4 znaków</div>}
            </div>

            <div>
              <select name='province' onChange={this.handleChange}>
                <option value='Wybierz Wojewodztwo'>Wybierz województwo</option>
                <option value="woj. dolnośląskie">woj. dolnośląskie</option>
                <option value="woj. kujawsko-pomorskie">woj. kujawsko-pomorskie</option>
                <option value="woj. lubelskie">woj. lubelskie</option>
                <option value="woj. lubuskie">woj. lubuskie</option>
                <option value="woj. łódzkie">woj. łódzkie</option>
                <option value="woj. małopolskie">woj. małopolskie</option>
                <option value="woj. mazowieckie">woj. mazowieckie</option>
                <option value="woj. opolskie">woj. opolskie</option>
                <option value="woj. podkarpackie">woj. podkarpackie</option>
                <option value="woj. podlaskie">woj. podlaskie</option>
                <option value="woj. pomorskie">woj. pomorskie</option>
                <option value="woj. śląskie">woj. śląskie</option>
                <option value="woj. świętokrzyskie">woj. świętokrzyskie</option>
                <option value="woj. warmińsko-mazurskie">woj. warmińsko-mazurskie</option>
                <option value="woj. wielkopolskie">woj. wielkopolskie</option>
                <option value="woj. zachodniopomorskie">woj. zachodniopomorskie</option>
              </select>
              {this.state.provinceValid ? <div className="valid valid-acces">{validAnswer}</div>
              : this.state.provinceValid ===''? null:<div className="valid valid-error">Wybierz województwo</div>}
            </div>
            
            <div>
              <select name='kindOfPlace' onChange={this.handleChange} >
                <option value='Wybierz rodzaj miejsca'>Wybierz rodzaj miejsca</option>
                <option value="Góry">Góry</option>
                <option value='Jezioro'>Jezioro</option>
                <option value='Rzeka'>Rzeka</option>
                <option value='Zabytki'>Zabytki</option>
                <option value='krajobraz'>krajobraz</option>
                <option value='Park'>Park</option>
                <option value='Ciekawe Miejsce'>Ciekawe Miejsce</option>
                <option value='Inne'>Inne</option>
              </select>
              {this.state.kindOfPlaceValid ? <div className="valid valid-acces">{validAnswer}</div>
              : this.state.kindOfPlaceValid ===''? null:<div className="valid valid-error">Wybierz rodzaj miejsca</div>}
            </div>

            <div>
              <textarea name='description' placeholder='Opis miejsca'
              onChange={this.handleChange}></textarea>
              {this.state.descriptionValid ? <div className="valid valid-acces">{validAnswer}</div>
              : this.state.descriptionValid ===''? null:<div className="valid valid-error">Opisz miejsce w min. 60 znakach</div>}
            </div>

            <div>
              <input type="text" name='cosik' placeholder='cos do dodania' data-error-message='rodzaj miejsca' 
              onChange={this.handleChange}/><div className="valid"></div>
            </div>

            <div className='addImageGroup'>

              <div className="addImage" 
              onClick={()=> this.fileInput0.click()}>
               {imagePreviewUrl1 ? <img src={imagePreviewUrl1}/> : <div><i className="fas fa-plus-circle"></i><p>DODAJ ZDJĘCIE</p></div>}
              <input name='imagePreviewUrl1' 
                onChange={this.handleSelectFile} 
                type='file'
                style={{display:'none'}}  multiple
                ref = {fileInput0 => this.fileInput0 = fileInput0}/>
              </div>

              <div className="addImage" onClick={()=> this.fileInput1.click()}>
              {imagePreviewUrl2 ? <img src={imagePreviewUrl2}/> : <div><i className="fas fa-plus-circle"></i><p>DODAJ ZDJĘCIE</p></div>}
              <input name='imagePreviewUrl2' 
                onChange={this.handleSelectFile} 
                type='file'
                style={{display:'none'}}  multiple
                ref = {fileInput1 => this.fileInput1 = fileInput1}/>
              </div>

              <div className="addImage" onClick={()=> this.fileInput2.click()}>
                {imagePreviewUrl3 ? <img src={imagePreviewUrl3}/> : <div><i className="fas fa-plus-circle"></i><p>DODAJ ZDJĘCIE</p></div>}
                <input name='imagePreviewUrl3' 
                onChange={this.handleSelectFile} 
                type='file'
                style={{display:'none'}}  multiple
                ref = {fileInput2 => this.fileInput2 = fileInput2}/>
              </div>

              <div className="addImage" onClick={()=> this.fileInput3.click()}>
                {imagePreviewUrl4 ? <img src={imagePreviewUrl4}/> : <div><i className="fas fa-plus-circle"></i><p>DODAJ ZDJĘCIE</p></div>}
                <input name='imagePreviewUrl4' 
                onChange={this.handleSelectFile} 
                type='file'
                style={{display:'none'}}  multiple
                ref = {fileInput3 => this.fileInput3 = fileInput3}/>
              </div>

              <div className="addImage" onClick={()=> this.fileInput4.click()}>
                {imagePreviewUrl5 ? <img src={imagePreviewUrl5}/> : <div><i className="fas fa-plus-circle"></i><p>DODAJ ZDJĘCIE</p></div>}
                <input name='imagePreviewUrl5' 
                onChange={this.handleSelectFile} 
                type='file'
                style={{display:'none'}}  multiple
                ref = {fileInput4 => this.fileInput4 = fileInput4}/>
              </div>

              <div className="addImage" onClick={()=> this.fileInput5.click()}>
               {imagePreviewUrl6 ? <img src={imagePreviewUrl6}/> : <div><i className="fas fa-plus-circle"></i><p>DODAJ ZDJĘCIE</p></div>}
                <input name='imagePreviewUrl6' 
                onChange={this.handleSelectFile} 
                type='file'
                style={{display:'none'}}  multiple
                ref = {fileInput5 => this.fileInput5 = fileInput5}/>
              </div>
              
            </div>


                 {/* <input name='file' 
                onChange={this.handleSelectFile} 
                type='file'
                style={{display:'none'}}  multiple
                ref = {fileInput => this.fileInput = fileInput}/>
                
                <Button label={'Dodaj zdjecie'} border={'gray'} color={'gray'} size={'bg'} type={'button'}
                onClick={()=> this.fileInput.click()}/>
                <div className="imgPreview">{$imagePreview}</div> */}

          </div>
          <Button label={'Wyslij'} type={'submit'} color='gray' border={'gray'} size={'sm'} top={10}/>
        </form>
      </div>
    )
  }
}