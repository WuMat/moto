import React from "react";
import { render } from "react-dom";
import { Provider } from "react-redux";
import { ConnectedRouter } from "react-router-redux";
import { Route } from 'react-router-dom';


import { history, store } from "./store";

import {Pages} from "pages";

import "./style/index.scss";





class App extends React.PureComponent {
  render() {
    return (
      <Provider store={store}>
        <ConnectedRouter history={history}>
          <Route component={Pages} />
        </ConnectedRouter>
      </Provider>
    )
  }
}

render(<App />, document.getElementById("root"));
