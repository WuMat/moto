import axios from 'axios';

const url = '/api/'

export const fetchPlace = (params) => {
  return axios.get(`${url}${params}`).then(response => response.data);
  
}