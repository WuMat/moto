const merge = require("webpack-merge");

const config = require("./config");
const webpackBase = require("./webpack.base");

const HTTP_PORT = 4000;

const next = {
  entry: [`webpack-dev-server/client?http://localhost:${HTTP_PORT}`, config.file.source],
  devServer: {
    contentBase: config.path.source,
    historyApiFallback: true,
    publicPath: "/",
    inline: true,
    host: "0.0.0.0",
    port: HTTP_PORT,
  },
};

const proxyConfig = {
  local: {
    "/api": {
      target: "http://localhost:8080/",
      pathRewrite: {
        "^/api": "/",
      },
    },
  },
};

module.exports = env => {
  const proxyFlag = env && env.proxy && proxyConfig.hasOwnProperty(env.proxy);
  const proxyName = proxyFlag ? env.proxy : "local";
  next.devServer.proxy = proxyConfig[proxyName];
  console.log(`Connected api ${Object.values(next.devServer.proxy)[0].target}`);
  return merge(webpackBase, next);
};
