const path = require("path");

const resolve = (...files) => path.resolve.apply(null, [__dirname, ".."].concat([...files]));

const config = {};

config.path = {
  source: resolve("src"),
  build: resolve("dist"),
  public: "/",
};

config.file = {
  html: path.join(config.path.source, "index.html"),
  source: path.join(config.path.source, "App.jsx"),
  client: "[name].[hash:6].js",
  // styles: "bundle.[hash:6].css",
};

module.exports = config;
