const { basename } = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

const config = require("./config");

module.exports = {
  resolve: {
    extensions: [".js", ".jsx"]
  },
  output: {
    filename: config.file.client,
    path: config.path.build,
    publicPath: config.path.public,
  },
  module: {
    loaders: [
      {
        test: /\.(js|jsx)$/,
        use: ["babel-loader"],
        exclude: /node_modules/
      },
      {
        enforce: "pre",
        test: /\.js$/,
        loader: "source-map-loader",
      },
      {
        test: /\.(png|jpg|jpeg|gif|svg)$/,
        loader: "url-loader",
        query: {
          limit: 8192,
          name: config.file.images,
        },
      },
      {
        test: /\.css$/,
        use: ["style-loader", "css-loader"],
      },
      {
        test: /\.scss$/,
        use: [
          {
            loader: "style-loader",
          },
          {
            loader: "css-loader",
          },
          {
            loader: "sass-loader",
            options: {
              includePaths: ["node_modules", config.path.source],
              outputStyle: "expanded",
              data: "$env: " + process.env.NODE_ENV + ";",
            },
          },
        ],
      },
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: config.file.html,
      path: config.path.build,
      filename: basename(config.file.html),
    })
  ],
  devtool: "cheap-module-source-map"
}
